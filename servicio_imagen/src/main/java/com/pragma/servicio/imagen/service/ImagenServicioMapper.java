package com.pragma.servicio.imagen.service;

import com.pragma.servicio.imagen.entity.Imagen;
import com.pragma.servicio.imagen.model.ImagenDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImagenServicioMapper {

    ImagenServicioMapper INSTANCIA = Mappers.getMapper(ImagenServicioMapper.class);

    Imagen toImagen (ImagenDTO imagenDTO);
    ImagenDTO toImagenDTO (Imagen imagen);
}
