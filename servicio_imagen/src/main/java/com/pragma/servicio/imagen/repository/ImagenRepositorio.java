package com.pragma.servicio.imagen.repository;

import com.pragma.servicio.imagen.entity.Imagen;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface ImagenRepositorio extends MongoRepository<Imagen,String> {
    public Imagen findByidImagen(String idImagen);
    public List<Imagen> findAllByidImagenIn(List<String> listadoIds);
}
