package com.pragma.servicio.imagen.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Base64;

@Document(collection = "imagenes")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Imagen {

    @Id
    @Field("id_imagen")
    private String idImagen;

    @Field("nombre_archivo")
    private String nombreArchivo;

    @Field("contenido_imagen")
    private byte[] contenidoImagen;

}
