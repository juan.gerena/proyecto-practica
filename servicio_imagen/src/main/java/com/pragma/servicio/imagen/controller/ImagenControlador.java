package com.pragma.servicio.imagen.controller;

import com.pragma.servicio.imagen.service.ImagenServicio;
import com.pragma.servicio.imagen.model.ImagenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/imagenes")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })
public class ImagenControlador {

    @Autowired
    private ImagenServicio imagenServicio;


    @GetMapping("/{idImagen}")
    public ResponseEntity<ImagenDTO> obtenerImagenPorId(@PathVariable String idImagen){

        ImagenDTO imagen = imagenServicio.obtenerImagen(idImagen);

        if(imagen == null){
            return  ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(imagen);
    }

    @GetMapping("/todas")
    public ResponseEntity<List<ImagenDTO>> obtenerTodasLasImagenes(){

        List<ImagenDTO> imagenes = imagenServicio.listarImagenes();

        if(imagenes.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(imagenes);
    }

    @PostMapping("/listado")
    public ResponseEntity<List<ImagenDTO>> obtenerTodasPorId(@RequestBody List<String> listadoId){

        /*List<ImagenDTO> imagenes = imagenServicio.listarImagenesPorId(listadoId);

        if(imagenes.isEmpty()){*/
            return ResponseEntity.noContent().build();
        /*}

        return ResponseEntity.ok(imagenes);*/
    }

    @PostMapping
    public ResponseEntity<ImagenDTO> guardarImagen(@RequestBody ImagenDTO nuevaImagen){

        ImagenDTO imagenCreada = imagenServicio.crearImagen(nuevaImagen);

        if(imagenCreada == null){
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(imagenCreada);
    }

    @PutMapping
    public ResponseEntity<ImagenDTO> actualizarImagen(@RequestBody ImagenDTO actualizacionImagen){

        ImagenDTO imagenActualizada = imagenServicio.actualizarImagen(actualizacionImagen);

        if(imagenActualizada == null){
            return ResponseEntity.noContent().build();
        }


        return ResponseEntity.ok(imagenActualizada);
    }

    @DeleteMapping
    public ResponseEntity<ImagenDTO> borrarImagenPorId(@RequestParam String idImagen){

        ImagenDTO imagenBorrada = imagenServicio.deleteImagen(idImagen);

        if(imagenBorrada == null){
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok().build();
    }


}
