package com.pragma.servicio.imagen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableSwagger2
@Configuration
public class ConfiguracionSwagger {

    @Bean
    public Docket clienteDocumentacion(){
        return new Docket(DocumentationType.SWAGGER_2).select()
                        .apis(RequestHandlerSelectors.basePackage("com.pragma.servicio.imagen.controller"))
                        .paths(PathSelectors.any()).build().apiInfo(informacionAPI());
    }

    private ApiInfo informacionAPI(){
        return new ApiInfo("Imagen API",
                           "Servicio REST para manejo CRUD de imagenes",
                           "1.0.0",
                           "",
                           new Contact("Juan Camilo Gerena","","juan.gerena@pragma.com.co"),
                           "","", Collections.emptyList());
    }

}
