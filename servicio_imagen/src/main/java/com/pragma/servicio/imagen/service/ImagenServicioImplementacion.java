package com.pragma.servicio.imagen.service;

import com.pragma.servicio.imagen.repository.ImagenRepositorio;
import com.pragma.servicio.imagen.entity.Imagen;
import com.pragma.servicio.imagen.model.ImagenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImagenServicioImplementacion implements ImagenServicio {

    @Autowired
    private ImagenRepositorio imagenRepositorio;


    @Override
    public List<ImagenDTO> listarImagenes() {

        List<Imagen> imagenesExistentes = imagenRepositorio.findAll();
        List<ImagenDTO> listadoImagenes = new ArrayList<>();


        if(!imagenesExistentes.isEmpty()){
            for(Imagen imagenEncontrada:imagenesExistentes){

                ImagenDTO imagen = ImagenServicioMapper.INSTANCIA.toImagenDTO(imagenEncontrada);

                listadoImagenes.add(imagen);
            }
        }

        return listadoImagenes;
    }

    @Override
    public List<ImagenDTO> listarImagenesPorId(List<String> identificaciones) {

        List<Imagen> imagenesExistentes = imagenRepositorio.findAllByidImagenIn(identificaciones);
        List<ImagenDTO> listadoImagenes = new ArrayList<>();


        if(!imagenesExistentes.isEmpty()){
            for(Imagen imagenEncontrada:imagenesExistentes){

                ImagenDTO imagen = ImagenServicioMapper.INSTANCIA.toImagenDTO(imagenEncontrada);

                listadoImagenes.add(imagen);
            }
        }

        return listadoImagenes;
    }

    @Override
    public ImagenDTO crearImagen(ImagenDTO imagen) {

        if(imagen != null){

            Imagen nuevaImagen = ImagenServicioMapper.INSTANCIA.toImagen(imagen);
            Imagen imagenCreada = imagenRepositorio.save(nuevaImagen);

            return ImagenServicioMapper.INSTANCIA.toImagenDTO(imagenCreada);
        }

        return null;
    }



    @Override
    public ImagenDTO obtenerImagen(String id) {

        ImagenDTO imagenEncontrada = ImagenServicioMapper.INSTANCIA.toImagenDTO(imagenRepositorio.findByidImagen(id));

        if(imagenEncontrada == null){
            return null;
        }

        return imagenEncontrada;
    }

    @Override
    public ImagenDTO actualizarImagen(ImagenDTO imagen) {

        if(imagen != null){
            String idImagen = ImagenServicioMapper.INSTANCIA.toImagen(imagen).getIdImagen();
            Imagen actualizacionImagen = imagenRepositorio.findByidImagen(idImagen);


            if(actualizacionImagen != null){
                actualizacionImagen.setNombreArchivo(imagen.getNombreArchivo());
                actualizacionImagen.setContenidoImagen(imagen.getContenidoImagen());

                Imagen imagenActualizada = imagenRepositorio.save(actualizacionImagen);

                return ImagenServicioMapper.INSTANCIA.toImagenDTO(imagenActualizada);
            }

        }

        return null;
    }

    @Override
    public ImagenDTO deleteImagen(String idImagen) {
        Imagen imagen = imagenRepositorio.findByidImagen(idImagen);

        if(imagen != null){
            imagenRepositorio.delete(imagen);
            return ImagenServicioMapper.INSTANCIA.toImagenDTO(Imagen.builder().build());
        }

        return null;

    }



}
