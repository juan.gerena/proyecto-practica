package com.pragma.servicio.imagen.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DetalleError {
    private String claseError;
    private String mensajeError;
    private Integer lineaError;
    private String metodoError;

}
