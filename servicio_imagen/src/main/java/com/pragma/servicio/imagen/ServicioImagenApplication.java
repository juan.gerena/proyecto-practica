package com.pragma.servicio.imagen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ServicioImagenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioImagenApplication.class, args);
	}

}
