package com.pragma.servicio.imagen.model;

import lombok.Builder;
import lombok.Data;

import java.util.Base64;

@Builder
@Data
public class ImagenDTO {

    private String idImagen;
    private String nombreArchivo;
    private byte[] contenidoImagen;

}
