package com.pragma.servicio.imagen.service;

import com.pragma.servicio.imagen.model.ImagenDTO;

import java.util.List;

public interface ImagenServicio {

    public List<ImagenDTO> listarImagenes();

    public List<ImagenDTO> listarImagenesPorId(List<String> identificaciones);

    public ImagenDTO crearImagen(ImagenDTO Imagen);

    public ImagenDTO obtenerImagen(String id);

    public ImagenDTO actualizarImagen(ImagenDTO Imagen);


    public ImagenDTO deleteImagen(String id);

}
