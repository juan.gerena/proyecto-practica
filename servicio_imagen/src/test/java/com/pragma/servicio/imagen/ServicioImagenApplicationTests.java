package com.pragma.servicio.imagen;


import com.pragma.servicio.imagen.controller.ImagenControlador;
import com.pragma.servicio.imagen.model.ImagenDTO;
import com.pragma.servicio.imagen.service.ImagenServicio;
import com.pragma.servicio.imagen.service.ImagenServicioMapper;
import org.junit.jupiter.api.BeforeEach;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.assertj.core.api.Assertions.assertThat;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


@ExtendWith(MockitoExtension.class)
class ServicioImagenApplicationTests {

	@InjectMocks
	ImagenControlador imagenControlador;

	@Mock
	ImagenServicio imagenServicio;

	@Mock
	ImagenServicioMapper mapper;

	@BeforeEach
	void setup(){

		lenient().when(imagenServicio.obtenerImagen("1"))
				.thenReturn(ImagenDTO
						.builder()
						.idImagen("1")
						.nombreArchivo("imagen falsa.jpg")
						.build());

	}


	@Test
	void consultarImagenExistente(){

		// given
		String idImagen = "1";

		// when
		ResponseEntity<ImagenDTO> resultado = imagenControlador.obtenerImagenPorId(idImagen);

		// then

		assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.OK);

	}

	@Test
	void consultarImagenNoExiste(){

		// given
		String idImagen = "123456789";

		// when
		ResponseEntity<ImagenDTO> resultado = imagenControlador.obtenerImagenPorId(idImagen);

		// then

		assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

	}



}
