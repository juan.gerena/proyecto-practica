package com.pragma.servicio.cliente.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ImagenDTO {

    private String idImagen;
    private String nombreArchivo;
    private byte[] contenidoImagen;

}
