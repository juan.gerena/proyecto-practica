package com.pragma.servicio.cliente.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pragma.servicio.cliente.client.ClienteImagen;
import com.pragma.servicio.cliente.entity.ClienteIdentificacion;
import com.pragma.servicio.cliente.model.ClienteConImagenDTO;
import com.pragma.servicio.cliente.model.ClienteDTO;
import com.pragma.servicio.cliente.model.ClienteIdentificacionDTO;
import com.pragma.servicio.cliente.model.ImagenDTO;
import com.pragma.servicio.cliente.repository.ClienteRepositorio;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import com.pragma.servicio.cliente.entity.Cliente;
import org.springframework.stereotype.Service;

@Service
public class ClienteServicioImplementacion implements ClienteServicio {

	@Autowired
	private ClienteRepositorio repositorioCliente;


	@Autowired
	ClienteImagen clienteImagen;
	
	
	@Override
	public List<ClienteDTO> obtenerTodosLosClientes() {
		
		List<Cliente> clientesEncontrados = repositorioCliente.findAll();
		List<ClienteDTO> clientes = new ArrayList<>();
		
		if(!clientesEncontrados.isEmpty()){
			for(Cliente clienteEncontrado:clientesEncontrados) {

				ClienteDTO infoCliente = ClienteServicioMapper.INSTANCIA.toClienteDto(clienteEncontrado);

				clientes.add(infoCliente);
			}
		}
		
		return clientes;
	}

	@Override
	public ClienteDTO obtenerCliente(ClienteIdentificacionDTO idCliente) {

		Cliente clienteEncontrado = repositorioCliente.findByIdCliente(new ClienteIdentificacion(idCliente.getNumeroDocumento(),idCliente.getTipoDocumento()));

		if(clienteEncontrado != null) {
			return  ClienteServicioMapper.INSTANCIA.toClienteDto(clienteEncontrado);
		}

		return null;
	}

	@Override
	public List<ClienteDTO> obtenerClientePorEdad(Integer edad) {

		List<Cliente> clientesEncontrados = repositorioCliente.findByEdadCliente(edad);
		List<ClienteDTO> infoClientes = new ArrayList<>();


		if(!clientesEncontrados.isEmpty()) {
			for (Cliente clienteEncontrado : clientesEncontrados) {
				ClienteDTO infoCliente = ClienteServicioMapper.INSTANCIA.toClienteDto(clienteEncontrado);
				infoClientes.add(infoCliente);
			}
		}


		return infoClientes;
	}

	@Override
	public List<ClienteDTO> obtenerClientesMayoresEdad(Integer edad) {

		List<Cliente> clientesEncontrados = repositorioCliente.findByEdadClienteGreaterThanEqual(edad);
		List<ClienteDTO> infoClientes = new ArrayList<>();


		if(!clientesEncontrados.isEmpty()) {
			for (Cliente clienteEncontrado : clientesEncontrados) {

				ClienteDTO infoCliente = ClienteServicioMapper.INSTANCIA.toClienteDto(clienteEncontrado);

				infoClientes.add(infoCliente);
			}
		}


		return infoClientes;
	}

	@Override
	public ClienteConImagenDTO obtenerClienteConImagen(ClienteIdentificacionDTO idCliente) {
		Cliente clienteEncontrado = repositorioCliente.findByIdCliente(
				ClienteServicioMapper
						.INSTANCIA
						.toClienteIdentificacion(idCliente));

		if(clienteEncontrado == null){
			return  null;
		}

		ImagenDTO imagenCliente = clienteImagen.obtenerImagenPorId(clienteEncontrado
				.getIdImagenCliente())
				.getBody();

		ClienteConImagenDTO infoClienteConImagen = ClienteServicioMapper
													.INSTANCIA
													.toClienteConImagenDto(clienteEncontrado,imagenCliente);

		return infoClienteConImagen;

	}


	@Override
	public ClienteDTO crearCliente(ClienteDTO cliente) {

		Cliente clienteNuevo = ClienteServicioMapper.INSTANCIA.toCliente(cliente);
		clienteNuevo.setFechaCreacion(new Date());
		return ClienteServicioMapper.INSTANCIA.toClienteDto(repositorioCliente.save(clienteNuevo));

	}

	@Override
	public ClienteDTO obtenerClienteConImagen(ClienteConImagenDTO clienteNuevo) {
		return null;
	}


	@Override
	public ClienteDTO actualizarCliente(ClienteDTO cliente) throws NotFoundException {

		Cliente actualizacionCliente = ClienteServicioMapper.INSTANCIA.toCliente(cliente);
		Cliente clienteBD = repositorioCliente.findByIdCliente(actualizacionCliente.getIdCliente());

		if(clienteBD == null){
			throw new NotFoundException("no se encontro el cliente a actualizar");
		}

		clienteBD.setNombreCliente(actualizacionCliente.getNombreCliente());
		clienteBD.setApellidoCliente(actualizacionCliente.getApellidoCliente());
		clienteBD.setEdadCliente(actualizacionCliente.getEdadCliente());
		clienteBD.setIdImagenCliente(actualizacionCliente.getIdImagenCliente());
		clienteBD.setCiudadCliente(actualizacionCliente.getCiudadCliente());


		return ClienteServicioMapper.INSTANCIA.toClienteDto(repositorioCliente.save(clienteBD));

	}

	@Override
	public ClienteDTO borrarCliente(ClienteDTO cliente) throws NotFoundException {

		Cliente clienteEliminado = repositorioCliente
				.findByIdCliente(ClienteServicioMapper
						.INSTANCIA
						.toClienteIdentificacion(cliente.getIdCliente()));


		if(clienteEliminado != null){
			repositorioCliente.delete(clienteEliminado);
		}else{
			throw new NotFoundException("No se encontro el cliente a eliminar");
		}

		return cliente;

	}

}
