package com.pragma.servicio.cliente.client;

import com.pragma.servicio.cliente.model.ImagenDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ClienteImagenHystrixFallBackFactory implements ClienteImagen{

    @Override
    public ResponseEntity<ImagenDTO> obtenerImagenPorId(String idImagen) {
        ImagenDTO imagenVacia = ImagenDTO.builder().build();
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(imagenVacia);
    }

    @Override
    public ResponseEntity<List<ImagenDTO>> obtenerTodasLasImagenes(){

        List<ImagenDTO> listaVacia = new ArrayList<>();

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(listaVacia);
    }

    @Override
    public ResponseEntity<List<ImagenDTO>> obtenerTodasPorId(List<String> listadoId) {
        List<ImagenDTO> listaVacia = new ArrayList<>();
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(listaVacia);
    }
}
