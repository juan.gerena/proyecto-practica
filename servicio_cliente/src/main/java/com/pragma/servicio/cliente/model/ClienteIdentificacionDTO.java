package com.pragma.servicio.cliente.model;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteIdentificacionDTO implements Serializable {

	private Integer numeroDocumento;
	private String tipoDocumento;

	private static final long serialVersionUID = 1L;
}
