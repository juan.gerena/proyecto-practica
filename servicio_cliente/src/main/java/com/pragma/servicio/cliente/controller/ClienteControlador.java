package com.pragma.servicio.cliente.controller;

import com.pragma.servicio.cliente.model.ClienteConImagenDTO;
import com.pragma.servicio.cliente.model.ClienteDTO;
import com.pragma.servicio.cliente.model.ClienteIdentificacionDTO;
import io.swagger.annotations.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pragma.servicio.cliente.service.ClienteServicio;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/clientes")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })
public class ClienteControlador {
	
	@Autowired
	private ClienteServicio servicioCliente;

	
	@GetMapping()
	@ApiOperation(value = "Encontrar cliente",notes = "Encontrar un cliente por numero identificacion y tipo de identificacion")
	@ApiResponses(value ={
			@ApiResponse(code = 204,message = "Cliente no encontrado"),
			@ApiResponse(code = 200,message = "Cliente encontrado")})
	public ResponseEntity<ClienteDTO> obtenerCliente(@RequestParam() Integer numeroDocumento, @RequestParam() String tipoDocumento){
		
		ClienteDTO infoCliente = servicioCliente.obtenerCliente(new ClienteIdentificacionDTO(numeroDocumento,tipoDocumento));

		if(infoCliente == null){
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(infoCliente);
	}

	@GetMapping("/todos")
	@ApiOperation(value = "Encontrar todos los clientes",notes = "Encontrar todos los clientes")
	@ApiResponses(value ={
			@ApiResponse(code = 204,message = "Clientes no encontrados"),
			@ApiResponse(code = 200,message = "Clientes encontrados")})
	public ResponseEntity<List<ClienteDTO>> obtenerTodosLosClientes(){

		List<ClienteDTO> infoClientes = servicioCliente.obtenerTodosLosClientes();

		if(infoClientes.isEmpty()){
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(infoClientes);
	}


	@GetMapping("/{edad}")
	@ApiOperation(value = "Encontrar clientes",notes = "Encontrar clientes por edad")
	@ApiResponses(value ={
			@ApiResponse(code = 204,message = "Clientes no encontrados"),
			@ApiResponse(code = 200,message = "Clientes encontrados")})
	public ResponseEntity<List<ClienteDTO>> obtenerClientePorEdad(@PathVariable Integer edad){

		List<ClienteDTO> infoClientes = servicioCliente.obtenerClientePorEdad(edad);

		if(infoClientes.isEmpty()){
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(infoClientes);
	}

	@GetMapping("/mayor/{edad}")
	@ApiOperation(value = "Encontrar clientes",notes = "Encontrar clientes mayores a cierta edad")
	@ApiResponses(value ={
			@ApiResponse(code = 204,message = "Clientes no encontrados"),
			@ApiResponse(code = 200,message = "Clientes encontrados")})
	public ResponseEntity<List<ClienteDTO>> obtenerClienteEdadMayorQue(@PathVariable Integer edad){

		List<ClienteDTO> infoClientes = servicioCliente.obtenerClientesMayoresEdad(edad);

		if(infoClientes.isEmpty()){
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(infoClientes);
	}

	@GetMapping("imagen")
	@ApiOperation(value = "Encontrar cliente",notes = "Encontrar cliente con su respectiva imagen")
	@ApiResponses(value ={
			@ApiResponse(code = 204,message = "Cliente no encontrado"),
			@ApiResponse(code = 200,message = "Cliente encontrado")})
	public ResponseEntity<ClienteConImagenDTO> obtenerClienteConImagen (@RequestParam() Integer numeroDocumento, @RequestParam() String tipoDocumento){

		ClienteConImagenDTO infoCliente = servicioCliente
											.obtenerClienteConImagen(
													ClienteIdentificacionDTO
															.builder()
															.numeroDocumento(numeroDocumento)
															.tipoDocumento(tipoDocumento).build());

		if(infoCliente== null){
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(infoCliente);
	}


	//@GetMapping("imagen/todos")
	/*@ApiOperation(value = "Encontrar clientes",notes = "Encontrar clientes con su respectiva imagen")
	@ApiResponses(value ={
			@ApiResponse(code = 204,message = "Clientes no encontrados"),
			@ApiResponse(code = 200,message = "Clientes encontrados")})
	public ResponseEntity<ClienteConImagenDTO> obtenerClienteConImagen (@RequestBody  ){

	}*/



	@PostMapping
	@ApiOperation(value = "Crear cliente",notes = "Crear cliente")
	@ApiResponses(value ={
			@ApiResponse(code = 201,message = "Clientes no encontrados"),
			@ApiResponse(code = 200,message = "Clientes encontrados")})
	public ResponseEntity<ClienteDTO> crearCliente(@RequestBody ClienteDTO clienteNuevo){

		ClienteDTO clienteCreado = servicioCliente.crearCliente(clienteNuevo);

		return ResponseEntity.status(HttpStatus.CREATED).body(clienteCreado);
	}


	@PutMapping
	public ResponseEntity<ClienteDTO> actualizarCLiente(@RequestBody ClienteDTO actuliazacionCliente) throws NotFoundException {

		ClienteDTO clienteActualizado = servicioCliente.actualizarCliente(actuliazacionCliente);

		return ResponseEntity.ok(clienteActualizado);

	}


	@DeleteMapping
	public ResponseEntity<ClienteDTO> borrarCliente (@RequestBody ClienteDTO cliente) throws NotFoundException {

		ClienteDTO clienteEliminado = servicioCliente.borrarCliente(cliente);

		return ResponseEntity.ok().build();

	}


}
