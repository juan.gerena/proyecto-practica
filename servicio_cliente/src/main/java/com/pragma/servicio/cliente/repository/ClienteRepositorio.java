package com.pragma.servicio.cliente.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.servicio.cliente.entity.Cliente;
import com.pragma.servicio.cliente.entity.ClienteIdentificacion;

public interface ClienteRepositorio extends JpaRepository<Cliente,Integer> {
    public Cliente findByIdCliente(ClienteIdentificacion idCliente);
    public List<Cliente> findByEdadCliente(Integer edad);
    public List<Cliente> findByEdadClienteGreaterThanEqual(Integer edad);
}
