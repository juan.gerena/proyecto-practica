package com.pragma.servicio.cliente.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClienteDTO {

	private ClienteIdentificacionDTO idCliente;
	private String nombreCliente;
	private String apellidoCliente;
	private Integer edadCliente;
	private CiudadDTO ciudadCliente;
	private String idImagenCliente;
	
	
}
