package com.pragma.servicio.cliente.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteIdentificacion implements Serializable {

    @Column(name = "numero_documento")
    private Integer numeroDocumento;

    @Column(name = "tipo_documento")
    private String tipoDocumento;

    private static final long serialVersionUID = 1L;

}
