package com.pragma.servicio.cliente.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CiudadDTO {

	private Integer idCiudad;
	private String nombreCiudad;
	
}
