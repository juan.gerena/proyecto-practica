package com.pragma.servicio.cliente.model;

import lombok.Data;


@Data
public class ClienteConImagenDTO {
    private ClienteIdentificacionDTO idCliente;
    private String nombreCliente;
    private String apellidoCliente;
    private Integer edadCliente;
    private CiudadDTO ciudadCliente;
    private ImagenDTO imagenCliente;
}
