package com.pragma.servicio.cliente.client;

import com.pragma.servicio.cliente.model.ImagenDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "servicio-imagen",path = "/imagenes",fallback = ClienteImagenHystrixFallBackFactory.class)
public interface ClienteImagen {

    @GetMapping("/{idImagen}")
    ResponseEntity<ImagenDTO> obtenerImagenPorId(@PathVariable String idImagen);

    @GetMapping("/todas")
    ResponseEntity<List<ImagenDTO>> obtenerTodasLasImagenes();

    @PostMapping("/listado")
    ResponseEntity<List<ImagenDTO>> obtenerTodasPorId(@RequestBody List<String> listadoId);
}
