package com.pragma.servicio.cliente.service;

import com.pragma.servicio.cliente.entity.Cliente;
import com.pragma.servicio.cliente.entity.ClienteIdentificacion;
import com.pragma.servicio.cliente.model.ClienteConImagenDTO;
import com.pragma.servicio.cliente.model.ClienteDTO;
import com.pragma.servicio.cliente.model.ClienteIdentificacionDTO;
import com.pragma.servicio.cliente.model.ImagenDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;

import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import org.springframework.web.bind.annotation.PostMapping;

@Mapper(componentModel = "spring")
public interface ClienteServicioMapper {

    ClienteServicioMapper INSTANCIA = Mappers.getMapper(ClienteServicioMapper.class);

    @Mapping(target = "fechaCreacion", ignore = true)
    Cliente toCliente(ClienteDTO clienteDto);
    ClienteDTO toClienteDto(Cliente cliente);
    ClienteIdentificacion toClienteIdentificacion(ClienteIdentificacionDTO idCliente);


    ClienteConImagenDTO toClienteConImagenDto(Cliente clienteEncontrado, ImagenDTO imagenCliente);
}
