package com.pragma.servicio.cliente.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "clientes")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cliente implements Serializable {

    @EmbeddedId
    private ClienteIdentificacion idCliente;

    @NotEmpty(message = "El nombre del cliente no debe ser vacio")
    @Column(name = "nombres",nullable = false,length = 30)
    private String nombreCliente;

    @NotEmpty(message = "El apellido del cliente no debe ser vacio")
    @Column(name = "apellidos",nullable = false,length = 30)
    private String apellidoCliente;

    @Positive(message = "La edad debe ser mayor a cero")
    @Column(name = "edad",length = 3)
    private Integer edadCliente;

    @NotNull(message = "La ciudad no puede ser vacia")
    @ManyToOne
    @JoinColumn(name = "id_ciudad",referencedColumnName = "id_ciudad",nullable = false)
    private Ciudad ciudadCliente;

    @Column(name = "id_imagen")
    private String idImagenCliente;

    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;

    private static final long serialVersionUID = 1L;

}
