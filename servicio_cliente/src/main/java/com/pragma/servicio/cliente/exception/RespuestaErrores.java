package com.pragma.servicio.cliente.exception;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RespuestaErrores extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> malaPeticion(Exception exception){

        DetalleError errorDetallado = DetalleError
                .builder()
                .claseError(exception.getClass().getName())
                .lineaError(exception.getStackTrace()[1].getLineNumber())
                .mensajeError(exception.getMessage())
                .metodoError(exception.getStackTrace()[1].getMethodName())
                .build();


        return new ResponseEntity<>(errorDetallado, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> notFound(Exception exception){
        DetalleError errorDetallado = DetalleError
                .builder()
                .claseError(exception.getClass().getName())
                .lineaError(exception.getStackTrace()[1].getLineNumber())
                .mensajeError(exception.getMessage())
                .metodoError(exception.getStackTrace()[1].getMethodName())
                .build();

        return new ResponseEntity<>(errorDetallado, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> errorGenerico(Exception exception){

        DetalleError errorDetallado = DetalleError
                .builder()
                .claseError(exception.getClass().getName())
                .lineaError(exception.getStackTrace()[1].getLineNumber())
                .mensajeError(exception.getMessage())
                .metodoError(exception.getStackTrace()[1].getMethodName())
                .build();

        return new ResponseEntity<>(errorDetallado, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
