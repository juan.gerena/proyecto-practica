package com.pragma.servicio.cliente.service;

import java.util.List;

import com.pragma.servicio.cliente.model.ClienteConImagenDTO;
import com.pragma.servicio.cliente.model.ClienteDTO;
import com.pragma.servicio.cliente.model.ClienteIdentificacionDTO;
import javassist.NotFoundException;


public interface ClienteServicio {

	
    public List<ClienteDTO> obtenerTodosLosClientes();
    public ClienteDTO obtenerCliente(ClienteIdentificacionDTO idCliente);
    public List<ClienteDTO> obtenerClientePorEdad(Integer edad);
    public List<ClienteDTO> obtenerClientesMayoresEdad(Integer edad);

    public ClienteConImagenDTO obtenerClienteConImagen(ClienteIdentificacionDTO idCliente);

    public ClienteDTO crearCliente(ClienteDTO cliente);
    ClienteDTO obtenerClienteConImagen(ClienteConImagenDTO clienteNuevo);

    public ClienteDTO actualizarCliente(ClienteDTO client) throws NotFoundException;
    public ClienteDTO borrarCliente(ClienteDTO client) throws NotFoundException;

}
