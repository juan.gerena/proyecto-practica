package com.pragma.servicio;

import com.pragma.servicio.cliente.controller.ClienteControlador;
import com.pragma.servicio.cliente.entity.Ciudad;
import com.pragma.servicio.cliente.entity.Cliente;
import com.pragma.servicio.cliente.entity.ClienteIdentificacion;
import com.pragma.servicio.cliente.model.CiudadDTO;
import com.pragma.servicio.cliente.model.ClienteDTO;
import com.pragma.servicio.cliente.model.ClienteIdentificacionDTO;
import com.pragma.servicio.cliente.service.ClienteServicio;
import com.pragma.servicio.cliente.service.ClienteServicioMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class ClienteControladorTest {

    @InjectMocks
    ClienteControlador clienteControlador;

    @Mock
    private ClienteServicio clienteServicio;

    @Mock
    private ClienteServicioMapper mapper;

    Cliente clienteFalso;

    @BeforeEach
    public void setup(){

        ClienteIdentificacionDTO id = new ClienteIdentificacionDTO(123,"CC");


        Mockito.when(mapper.toClienteIdentificacion(id))
                .thenReturn(ClienteIdentificacion
                        .builder()
                        .numeroDocumento(id.getNumeroDocumento())
                        .tipoDocumento(id.getTipoDocumento())
                        .build());

        clienteFalso = Cliente
                .builder()
                .idCliente(mapper.toClienteIdentificacion(id))
                .nombreCliente("juan camilo")
                .apellidoCliente("gerena burgos")
                .edadCliente(26)
                .ciudadCliente(Ciudad.builder().build())
                .idImagenCliente("1")
                .fechaCreacion(new Date())
                .build();



        Mockito.lenient().when(clienteServicio.obtenerCliente(id)).thenReturn(ClienteDTO
                        .builder()
                        .idCliente(id)
                        .nombreCliente(clienteFalso.getNombreCliente())
                        .apellidoCliente(clienteFalso.getApellidoCliente())
                        .ciudadCliente(CiudadDTO.builder().build())
                        .edadCliente(clienteFalso.getEdadCliente())
                        .idImagenCliente(clienteFalso.getIdImagenCliente())
                        .build());



    }



    @Test
    void obtenerClienteQueExiste(){

        // given

        ClienteIdentificacionDTO idPrueba = new ClienteIdentificacionDTO(123,"CC");

        // when
        ResponseEntity<ClienteDTO> resultado = clienteControlador
                                            .obtenerCliente(idPrueba.getNumeroDocumento(),
                                                            idPrueba.getTipoDocumento());

        // then

        assertThat(resultado.getStatusCode())
                .isEqualTo(HttpStatus.OK);

        assertThat(Objects.requireNonNull(resultado.getBody())
                .getNombreCliente()).isEqualTo(clienteFalso.getNombreCliente());

    }


    @Test
    void obtenerClienteQueNoExiste(){

        // given
        ClienteIdentificacionDTO idPrueba = new ClienteIdentificacionDTO(123,"TI");

        // when
        ResponseEntity<ClienteDTO> resultado = clienteControlador
                .obtenerCliente(idPrueba.getNumeroDocumento(),idPrueba.getTipoDocumento());

        // then
        assertThat(resultado.getStatusCode())
                .isEqualTo(HttpStatus.NO_CONTENT);


        assertThat(resultado.getBody()).isNull();

    }

    @Test
    void crearClienteNull(){
        // given
        ClienteDTO clienteVacio = null;

        // when
        ResponseEntity<ClienteDTO> resultado = clienteControlador.crearCliente(clienteVacio);

        // then
        assertThat(resultado.getBody()).isNull();
        assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    @Test
    void crearCliente(){
        // given
        ClienteDTO clienteDTO = ClienteDTO.builder().build();

        // when
        ResponseEntity<ClienteDTO> respuesta = clienteControlador.crearCliente(clienteDTO);

        // then
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.CREATED);

    }


}